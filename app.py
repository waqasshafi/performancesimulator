import numpy as np
from flask import Flask, request, jsonify, render_template
import pickle
import pandas as pd
from sklearn import preprocessing
from sklearn.preprocessing import MinMaxScaler
import os
from collections import OrderedDict

app = Flask(__name__)
APP_ROOT = os.path.dirname(os.path.abspath(__file__))
path = APP_ROOT+'/trainedModels/RandomForest/'

@app.route('/')
def home():
	return render_template('index.html', extra_info=[])

@app.route('/predict',methods=['POST'])
def predict():
	#int_features = [float(x) for x in request.form.values()]
	#confidenceIntervalPrediction = int_features[0] #first element in list is prediction interval percentile
	#int_features = int_features[1:] #the other elemet from beyond will be used to prediction
	#print(confidenceIntervalPrediction,int_features)

	prediction_values_dict = OrderedDict()
	int_features = []  # the other elemet from beyond will be used to prediction
	form_values = OrderedDict(request.form)
	confidenceIntervalPrediction = int(form_values['confidence_interval'])  #prediction interval percentile
	no_of_instances = int(form_values['no_of_instances'])

	form_index = 1
	for input_name, input_value in form_values.items():
		if "feature" in input_name:
			int_features.append(round((float(form_values['feature_'+str(form_index)])/no_of_instances),4))
			form_index+=1
	print(confidenceIntervalPrediction, no_of_instances, int_features)

	final_features = [np.array(int_features)]
	data_df = pd.DataFrame.from_dict(final_features)

	all_trained_models = os.listdir(path)
	for trained_model_name in all_trained_models:

		model = pickle.load(open(path+trained_model_name, 'rb'))
		prediction = model.predict(data_df)

		# get prediction with intervals
		predictionIntervals = prediction_intervals(model, data_df, prediction, percentile=confidenceIntervalPrediction)


		#create column names
		trained_model_name = trained_model_name.replace("_", " ")
		trained_model_name = trained_model_name.replace("-", " ")

		prediction_values_dict[os.path.splitext(trained_model_name)[0]] = predictionIntervals

		extra_info = OrderedDict()
		extra_info['load_select_value'] = str(int(int_features[0]))
	return render_template('index.html', prediction_data=prediction_values_dict, extra_info=extra_info)

def prediction_intervals(model, input_data, real_predicted_value, percentile=95):
	data = OrderedDict()
	predictions = np.array([pred.predict(input_data) for pred in model.estimators_])
	err_down = np.apply_along_axis(lambda x: np.percentile(x, (100 - percentile) / 2.), 0, predictions)
	err_up = np.apply_along_axis(lambda x: np.percentile(x, 100 - (100 - percentile) / 2.), 0, predictions)

	data['lower'] = round(err_down[0], 2)
	data['prediction'] = round(real_predicted_value[0], 2)
	data['upper'] = round(err_up[0], 2)
	return data

	# input_dict = {}
	# form_data = request.form
	# for key, value in form_data.items():
	# 	input_dict[key] = [float(value)]
	# df = pd.DataFrame(data)
	# x = df.values.astype(float)

	# # Create a minimum and maximum processor object
	# min_max_scaler = preprocessing.MinMaxScaler(feature_range=(0, 1))
	# x_scaled = min_max_scaler.fit_transform(x)
	# df_normalized = pd.DataFrame(x_scaled)
	# print(df_normalized)

if __name__ == '__main__':
    app.run(debug=True)
