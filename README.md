# Performance Simulator

This project contains code associated with a machine learning-based performance simulator that can allow early and less costly evaluation of the performance of large scale real-time systems.


System architecture
--------------------------
The system consist of 4 modules. Below is an overview of the communication channels and system structure.

``` wiki
 [Data Preprocessing]
         ⭣
  [Machine Learning]
         ⭣
      (Pickle)
         ⭣
    [Predictor]
         ⭣
    [Front-End]
```

- `Data Preprocessing`: This module contains an extracted data file which is used to data manipulation and later in the training process.
- `Machine Learning`: The module responsible for holding the algorithms and trains the ML model which will store in `.pickle` form later.
- `Predictor`: This module loads the trained `pickle` model to perform predictions by using the given data, which comes from simulator front-end.
- `Front-End`: This is the front end for all user activities(interactions, operations, actions).


Running Performance Simulator Locally.
---------------------------------
To run the components locally, following steps need to be followed:

1. For run the simulator we have to install the depencies librarries with this command under root directory of the project
  ```$ pip install -r requirements.txt```
  
    **Note:** If you want the installation of dependies not globally but in virtual environment, this source could help you with this
    ```
    For Windows: https://youtu.be/APOPm01BVrk?t=671
    For Mac/Linux: https://youtu.be/Kg1Yvry_Ydk?t=352
    ```
    
2. To run the simulator you need to run the following command (Default port is :5000, you can change it by adding --port=123 argument) and simulator can access via brower
    ```$ python app.py```
    
**Note:**
```
1. You can find "DataFile" under root Directory of Simulator which is named as "Final_Data.xlsx".
2. This command just need to run once when you need to train the model `$ python model.py`
```