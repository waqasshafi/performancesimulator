import pickle
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import datetime
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder, LabelEncoder, MinMaxScaler
from sklearn.ensemble import RandomForestRegressor
from pathlib import Path
import os
from collections import OrderedDict

APP_ROOT = os.path.dirname(os.path.abspath(__file__))

# global variables (configuration)
input_cols = ['vmp-sim_DN_traffic', 'vmp-sim_DR_traffic', 'vmp-sim_MN_traffic', 'vmp-sim_MR_traffic',
              'vmp-sim_PN_traffic', 'vmp-sim_SN_traffic', 'vmp-sim_SR_traffic', 'vmp-sim_VN_traffic',
              'vmp-sim_VR_traffic']

prediction_column_matching_string = "Charging Core"  # matching string (like query)
prediction_column_exact_match = ["CPU Utilization_vCPU-Used-Percentage"]

target = "CPU Utilization_vCPU-Used-Percentage"

# load dataset file
df = pd.read_excel('Final_Data.xlsx')

# *--------------------------**********-------------------------*
# *--------------------------CLEAN DATA-------------------------*
# *--------------------------**********-------------------------*

def removeOutliers(outlier_columns, df, fence_limit_interval_lower,fence_limit_interval_upper):
    # finding outliers in data (Outlier detection using quantile IQR method)
    GroupData = df.groupby('Load')

    #outlier_columns = ["Policy Control Response Time Charging Core", "CPU Utilization_vCPU-Used-Percentage","Online Consumption Response Time Charging Core", "Offline Consumption Response Time Charging Core"]
    Q1 = GroupData[outlier_columns].quantile(0.25)
    Q3 = GroupData[outlier_columns].quantile(0.75)
    IQR = Q3 - Q1

    # To set the limit of lower and upper Fence of the data
    Lower_Fence = Q1 - (fence_limit_interval_lower * IQR)
    Upper_Fence = Q3 + (fence_limit_interval_upper * IQR)

    # Extracted data after removal of outlier with defined range
    range_of_lu_fence = OrderedDict()
    for outlier_singleColumn in outlier_columns:
        # Range of Lower and Upper limit against load
        range_of_lu_fence[outlier_singleColumn] = OrderedDict()
        for load, value in Lower_Fence[outlier_singleColumn].iteritems():
            result = Upper_Fence[outlier_singleColumn][load]
            range_of_lu_fence[outlier_singleColumn][load] = [value, result]

    # dropped outliers data
    Extracted_Data = df

    dropped_indexes = []
    for row_index, row in Extracted_Data.iterrows():
        record_load = row['Load']

        for outlier_singleColumn in outlier_columns:
            column_value = row[outlier_singleColumn]

            lowerFence = range_of_lu_fence[outlier_singleColumn][record_load][0]
            upperFence = range_of_lu_fence[outlier_singleColumn][record_load][1]

            if (column_value < lowerFence) or (column_value > upperFence):
                dropped_indexes.append(row_index);
                if row_index in Extracted_Data.index:
                    Extracted_Data.drop(row_index, inplace=True)

    # reindex Extracted Data
    Extracted_Data = Extracted_Data.reset_index(drop=True)
    return Extracted_Data

Extracted_Data = removeOutliers(["Policy Control Response Time Charging Core", "Online Consumption Response Time Charging Core", "Offline Consumption Response Time Charging Core"], df, 1.5, 1.5)
Extracted_Data = removeOutliers(["CPU Utilization_vCPU-Used-Percentage"], Extracted_Data, 2, 3)

# change the timestamp foramt
to_timestamp_fct = lambda x: datetime.datetime.strptime(str(x), '%Y-%m-%d %H:%M:%S').timestamp()
Extracted_Data['timestamp'] = Extracted_Data['timestamp'].apply(to_timestamp_fct)

# handling nan column value (update N/A with mean of column)
Extracted_Data = Extracted_Data.fillna(Extracted_Data.mean())

# output column selection finalize
colNames = Extracted_Data.columns[Extracted_Data.columns.str.contains(pat=prediction_column_matching_string)]
colNames = colNames.append(pd.Index(prediction_column_exact_match))

# Label Encoding and OneHot Encoding (#convert string value to integer)
clone_input_cols = input_cols
for column in Extracted_Data.columns:
    if column in clone_input_cols:
        if Extracted_Data[column].dtype == type(object):
            le = LabelEncoder()
            Extracted_Data[column] = le.fit_transform(Extracted_Data[column])

            oneHotEncoder = OneHotEncoder()
            splitEncodedColumns = oneHotEncoder.fit_transform(Extracted_Data[column].values.reshape(-1, 1)).toarray()

            makeAndConcatCols = pd.DataFrame(splitEncodedColumns, columns=[column + "_" + str(int(i)) for i in
                                                                           range(splitEncodedColumns.shape[1])])
            Extracted_Data = pd.concat([Extracted_Data, makeAndConcatCols], axis=1)

            # drop orginal column
            Extracted_Data = Extracted_Data.drop(column, 1)
            clone_input_cols.remove(column)

            for extColumn in range(splitEncodedColumns.shape[1]):
                clone_input_cols.append(column + "_" + str(int(extColumn)))

Extracted_Data = Extracted_Data.dropna()
input_data = Extracted_Data[clone_input_cols]
input_data = input_data.round(4)

# # scaling and normalization of data
# minMaxScaler = MinMaxScaler(feature_range=(0, 1))
# input_data = minMaxScaler.fit_transform(input_data)
# input_data = pd.DataFrame(data=input_data)


# *--------------------------**********-------------------------*
# *--------------------------CLEAN DATA-------------------------*
# *--------------------------**********-------------------------*

# initialize and fit model
# output_data = Extracted_Data[colNames]
for colName in colNames.tolist():
    output_data = Extracted_Data[colName]
    # output_data = Extracted_Data[target]
    # input_data_train, input_data_test, output_data_train, output_data_test = train_test_split(input_data, output_data, test_size = 0.3)

    # model selection
    # random Forest
    model = RandomForestRegressor(max_depth=25, n_estimators=1000, random_state = 1, min_samples_split = 5,
                               max_features = "auto")
    # model.fit(input_data_train, output_data_train);
    model.fit(input_data, output_data);

    selectedModel = 'RandomForest'
    # modeling files directory
    modelsDirectory = APP_ROOT + '/trainedModels/' + selectedModel + '/'
    # serializing the model using pickle
    Path(modelsDirectory).mkdir(parents=True, exist_ok=True)
    pickle.dump(model, open(modelsDirectory + colName.replace(" ", "_") + '.pkl', 'wb'))
